var ca= new Audio();
ca.src="music/325805__wagna__collect.wav";
var ea= new Audio();
ea.src="music/142608__autistic-lucario__error.wav";
var score=0;
var gameScreen = $('#main');

var random_images_array2 = ["image/wrong.png", "image/wrong2.png", "image/wrong3.png"];
var random_images_array = ["image/correctfull.png", "image/correctfull2.png"];
function getRandomImage(imgAr) {
    var num = Math.floor( Math.random() * imgAr.length );
    var imgSrc = imgAr[ num ];
    return imgSrc;
}


window.randomImage = function(game) {
	
	
    var elem = document.createElement("img");
	var ship=$(elem);
    var container = document.getElementById("main");
    var divWidth = $("#main").innerWidth()-150;
  	var divHeight = $("#main").innerHeight()-207;
  	var randWidth = Math.floor(Math.random() * divWidth)+220+'px';
  	var randHeight = Math.floor(Math.random() * divHeight)+111+'px';
	elem.src = getRandomImage(random_images_array);
	elem.setAttribute("height", "137.5");
	elem.setAttribute("width", "100");
	elem.style.position = 'absolute';
	elem.style.left = randWidth;
	elem.style.top = randHeight;
	elem.style.transition = "all 2s";
    container.appendChild(elem);
	ship.click(function() {
	scdisplay();
	elem.classList.remove("animate3");
	void elem.offsetWidth;
	elem.classList.add('animate3');
	playwrong();
	score--;
  });
	setTimeout(function(){
  	ship.addClass( "animate-me" );
  }, 16 );
}

window.randomImage2 = function(game) {
	
	
    var elem = document.createElement("img");
	var ship=$(elem);
    var container = document.getElementById("main");
    var divWidth = $("#main").innerWidth()-150;
  	var divHeight = $("#main").innerHeight()-207;
  	var randWidth = Math.floor(Math.random() * divWidth)+220+'px';
  	var randHeight = Math.floor(Math.random() * divHeight)+111+'px';
	var randZ = Math.floor(Math.random()*2000)+'px';
	elem.src = getRandomImage(random_images_array2);
	elem.setAttribute("height", "137.5");
	elem.setAttribute("width", "100");
	elem.style.position = 'absolute';
	elem.style.left = randWidth;
	elem.style.top = randHeight;
	elem.style.transition = "all 2s";
    container.appendChild(elem);
	ship.click(function() {
	scdisplay();
	playcorrect();	
	score++;	
  	this.remove()
  });
	setTimeout(function(){
  	ship.addClass( "animate-me" );
  }, 16 );
}

function scdisplay() {
    document.getElementById('score').innerHTML = "Score: "+score;
}

function setscore(){
	score=0;
}

function playcorrect(){
	ca.play();
}

function playwrong(){
	ea.play();
}
