var CCOUNT;
$(document).ready(function () {
        CCOUNT = 45;
        cdreset();

});
var t, count;

function cddisplay() {
    document.getElementById('timespan').innerHTML = "Remaining Time: "+count+"s";
}

function countdown() {
    cddisplay();
	off3();
    if (count === 0) {
        document.getElementById('timespan').innerHTML = "STOP";
		removeImage();
		scdisplay()
    } 
	else if ((count >= 30)){
		on2();
		count--;
		t = setTimeout(countdown, 1000);
	}
	else {
		off2();
		
		randomImage();
		randomImage2();
        count--;
        t = setTimeout(countdown, 1000);
    }
}

function cdpause() {
    clearTimeout(t);
}

function cdreset() {
	document.getElementsByClassName('scene')[0].classList.remove("animate");
	document.getElementsByClassName('scene')[0].classList.remove("animate2");
	setscore();
	scdisplay();
    cdpause();
    count = CCOUNT;
    cddisplay();
	removeImage();
	on3();
}

function removeImage() {
    var element = document.getElementById("main");
	while (element.firstChild) {
  element.removeChild(element.firstChild);
}
}

function on() {
    document.getElementById("overlay").style.display = "block";
}

function off() {
    document.getElementById("overlay").style.display = "none";
}

function on2() {
    document.getElementById("overlay2").style.display = "block";
}

function off2() {
    document.getElementById("overlay2").style.display = "none";
}

function on3() {
    document.getElementById("overlay3").style.display = "block";
}

function off3() {
    document.getElementById("overlay3").style.display = "none";
}

var $body = $( 'body' );
var $scene = $( '.scene' );
var rX = 0;
var rY = 0;
$body.on( 'mousedown', function() { $body.on( 'mousemove', rotateScene ) } );
$body.on( 'mouseup', function() { $body.off( 'mousemove', rotateScene ) } );
function rotateScene( e ) {
	rY += e.originalEvent.movementX/2;
	rX -= e.originalEvent.movementY/2; 
	$scene.css( 'transform', 'rotateX('+ rX +'deg) rotateY('+ rY +'deg)' );
}

$( '.scene' ).prop( 'draggable', false );

function turnback() { 
	document.getElementsByClassName('scene')[0].classList.remove("animate");
	void document.getElementsByClassName('scene')[0].offsetWidth;
	document.getElementsByClassName('scene')[0].classList.add('animate');
}

function turnside() { 
	document.getElementsByClassName('scene')[0].classList.remove("animate2");
	void document.getElementsByClassName('scene')[0].offsetWidth;
	document.getElementsByClassName('scene')[0].classList.add('animate2');
}